﻿using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Windows.Controls;
using Newtonsoft.Json;

namespace ru.tsb.sbyt.outlook.sl
{
    public partial class MainPage : UserControl
    {
        private readonly Dictionary<String, byte[]> attachments = new Dictionary<String, byte[]>();
        private String body;
        private String from;
        private String subject;
        private String to;
        private String direction;

        public MainPage()
        {
            InitializeComponent();
            try
            {
                // обращаемся к объекту текущего письма 
                // (если будут выделены несколько писем то первое попавшееся)
                var letter = (ScriptObject)HtmlPage.Window.GetProperty("external");
                if (letter == null) return;
                to = letter.GetProperty("to") as string;
                @from = letter.GetProperty("from") as string;
                subject = letter.GetProperty("subject") as string;
                body = letter.GetProperty("body") as string;
                direction = letter.GetProperty("direction") as string;
                var raw =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(
                        letter.GetProperty("attachments") as string);
                foreach (var item in raw)
                {
                    attachments.Add(item.Key, Convert.FromBase64String(item.Value));
                }
            }
            catch (Exception ex)
            {
                // we should send info to loggly
            }
        }
    }
}