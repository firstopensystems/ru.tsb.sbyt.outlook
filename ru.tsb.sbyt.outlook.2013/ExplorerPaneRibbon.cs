﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Ribbon;

namespace ru.tsb.sbyt.outlook._2013
{
    public partial class ExplorerPaneRibbon
    {
        private void ExplorerPaneRibbon_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            // Check to see if a item is select in explorer or we are in inspector.
            if (e.Control.Context is Inspector)
            {
                var inspector = (Inspector)e.Control.Context;

                if (inspector.CurrentItem is MailItem)
                {
                    var form = new ExternalFormSbyt(inspector.CurrentItem as MailItem);
                    form.Show();
                }
            }

            if (e.Control.Context is Explorer)
            {
                var explorer = (Explorer)e.Control.Context;

                var selectedItems = explorer.Selection;
                if (selectedItems.Count != 1)
                {
                    return;
                }

                if (selectedItems[1] is MailItem)
                {
                    var form = new ExternalFormSbyt(selectedItems[1] as MailItem);
                    form.Show();
                }
            }
        }

        private void ConfigSbytBtn_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new ConfigForm();
            form.Show();
        }
    }
}
