﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("УАП ОИК «Сбыт»")]
[assembly: AssemblyDescription("Передача сообщений в Модуль УАП ОИК «Сбыт»")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ООО Компания Техносбыт")]
[assembly: AssemblyProduct("ru.tsb.sbyt.outlook.2013")]
[assembly: AssemblyCopyright("Copyright © Техносбыт 2015")]
[assembly: AssemblyTrademark("УАП ОИК «Сбыт»")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("460e8d3b-6622-4b7b-bceb-c0f57566ce8b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]

