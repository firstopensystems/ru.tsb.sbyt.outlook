﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using Newtonsoft.Json;

namespace ru.tsb.sbyt.outlook._2013
{
    [ComVisible(true)]
    public partial class ExternalFormSbyt : Form
    {
        public ExternalFormSbyt(MailItem mail)
        {
            InitializeComponent();
            webBrowser1.ObjectForScripting = this;
            to = "";
            for (int count = 1; count <= mail.Recipients.Count; count++)
            {
                mail.Recipients[count].Resolve();
                to += "," + GetPrimarySMTPAddress(mail.Recipients[count].AddressEntry);
            }
            to = to.Substring(1);
            from = GetPrimarySMTPAddress(mail.Sender);
            direction = "in";
            foreach (Account acc in ThisAddIn.Accounts)
            {
                if (acc.SmtpAddress == from)
                {
                    direction = "out";
                    break;
                }
            }
            subject = mail.Subject;
            body = mail.Body;
            _attachments = new Dictionary<string, string>();
            for (int count = 1; count <= mail.Attachments.Count; count++)
            {
                if (String.IsNullOrEmpty(mail.Attachments[count].FileName) || mail.Attachments[count].Size <= 0)
                    continue;
                string path = Path.Combine(Path.GetTempPath(), mail.Attachments[count].FileName);
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                mail.Attachments[count].SaveAsFile(path);
                _attachments.Add(mail.Attachments[count].FileName, Convert.ToBase64String(File.ReadAllBytes(path)));
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            webBrowser1.Navigate(ConfigurationManager.AppSettings["sbytEndpoint"]);
        }

        public static String GetPrimarySMTPAddress(AddressEntry addressEntry)
        {
            if (!"ex".Equals(addressEntry.Type.ToLower())) return addressEntry.Address;
            return addressEntry.DisplayType == OlDisplayType.olDistList ?
                addressEntry.GetExchangeDistributionList().PrimarySmtpAddress :
                addressEntry.GetExchangeUser().PrimarySmtpAddress;
        }

        public string to { get; set; }
        public string from { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string direction { get; set; }

        public string attachments
        {
            get { return JsonConvert.SerializeObject(_attachments); }
        }

        private Dictionary<string, string> _attachments { get; set; }
    }
}