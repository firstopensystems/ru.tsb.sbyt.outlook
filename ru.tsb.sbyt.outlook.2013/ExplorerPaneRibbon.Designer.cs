﻿namespace ru.tsb.sbyt.outlook._2013
{
    partial class ExplorerPaneRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ExplorerPaneRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.button1 = this.Factory.CreateRibbonButton();
            this.ConfigSbytBtn = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.ControlId.OfficeId = "TabMail";
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "TabMail";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.button1);
            this.group1.Label = "УАП ОИК «Сбыт»";
            this.group1.Name = "group1";
            // 
            // button1
            // 
            this.button1.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.button1.Image = global::ru.tsb.sbyt.outlook._2013.Properties.Resources.tab_convert_export;
            this.button1.Label = "Передать";
            this.button1.Name = "button1";
            this.button1.ScreenTip = "Передача сообщений в Модуль УАП ОИК «Сбыт»";
            this.button1.ShowImage = true;
            this.button1.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button1_Click);
            // 
            // ConfigSbytBtn
            // 
            this.ConfigSbytBtn.Label = "Настройка Сбыт";
            this.ConfigSbytBtn.Name = "ConfigSbytBtn";
            this.ConfigSbytBtn.ShowImage = true;
            this.ConfigSbytBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ConfigSbytBtn_Click);
            // 
            // ExplorerPaneRibbon
            // 
            this.Name = "ExplorerPaneRibbon";
            // 
            // ExplorerPaneRibbon.OfficeMenu
            // 
            this.OfficeMenu.Items.Add(this.ConfigSbytBtn);
            this.RibbonType = "Microsoft.Outlook.Explorer";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.ExplorerPaneRibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ConfigSbytBtn;
    }

    partial class ThisRibbonCollection
    {
        internal ExplorerPaneRibbon ExplorerPaneRibbon
        {
            get { return this.GetRibbon<ExplorerPaneRibbon>(); }
        }
    }
}
